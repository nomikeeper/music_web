module.exports = [{
    entry: {
       index: "./public/React/js/index.js",
       news: "./public/React/js/News.js",
    },
    output:{
        path: __dirname + "/public/React/bundle/",
        filename: '[name]_bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query:{
                    presets:[ "react" ]
                }, 
            }
        ]
    }
}]
package models

import (
	"time"
)

type User struct {
	UserID      int64 `db:"user_id"`
	Username    string
	Password    string
	FirstName   string
	LastName    string
	DateOfBirth time.Time
	UpdatedAt   time.Time
	CreatedAt   time.Time
}

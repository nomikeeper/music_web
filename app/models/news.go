package models

type News struct {
	NewsID    int64  `db:"news_id" json:"news_id"`
	Title     string `json:"news_title"`
	Context   string `json:"news_context"`
	ImagePath string `db:"image_url" json:"news_image_path"`
	CreatedAt string `json:"news_created_at"`
	UpdatedAt string `json:"news_updated_at"`
	CreatedBy string `json:"news_created_by"`
	UpdatedBy string `json:"news_updated_by"`
}

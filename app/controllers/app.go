package controllers

import (
	"github.com/revel/revel"
)

type App struct {
	*revel.Controller
}

// App Index page
func (c App) Index() revel.Result {
	return c.Render()
}

// Events page
func (c App) Events() revel.Result {
	return c.Render()
}

// App Index page
func (c App) Blog() revel.Result {
	return c.Render()
}

// About page
func (c App) About() revel.Result {
	return c.Render()
}

// Media page
func (c App) Media() revel.Result {
	return c.Render()
}

// Other page
func (c App) Other() revel.Result {
	return c.Render()
}

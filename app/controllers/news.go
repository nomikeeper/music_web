package controllers

import (
	"bytes"
	"fmt"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"log"
	"music_web/app/models"
	"os"
	"time"

	app "music_web/app"

	"github.com/revel/revel"
)

type FileInfo struct {
	ContentType string
	Filename    string
	RealFormat  string `json:",omitempty"`
	Resolution  string `json:",omitempty"`
	ImagePath   string
	Size        int
	Status      string `json:",omitempty"`
}

const (
	_      = iota
	KB int = 1 << (10 * iota)
	MB
	GB
)

type NewsController struct {
	*revel.Controller
}

func (c NewsController) Index(limit int) revel.Result {
	return c.Render()
}

func (c NewsController) FetchNews(page int) revel.Result {
	var fetchedNewsArray []models.News
	data := make(map[string]interface{})
	CurrentPage := 1
	if page != CurrentPage && page > 0 {
		CurrentPage = page
	}
	query := fmt.Sprintf("SELECT * FROM news ORDER BY UpdatedAt DESC LIMIT %d, 5", ((CurrentPage - 1) * 5))
	_, err := app.Dbmap.Select(&fetchedNewsArray, query, models.News{})
	app.CheckErr(err, "Failed to fetch news from DB.")
	count, err := app.Dbmap.SelectInt("SELECT count(*) FROM news")
	app.CheckErr(err, "Failed to fetch news from DB.")

	data["error"] = nil
	data["total_count"] = count
	data["news_list"] = fetchedNewsArray

	return c.RenderJSON(data)
}

func (c NewsController) AddNews(imgFile []byte) revel.Result {
	f := c.Params.Form
	fmt.Println("SOME FORM >>>>>>>>>>>>>>>>>>>>>>>>>>>> ", f)

	var news models.News
	// Checkin the image and copying it

	// Validation rules.
	if imgFile != nil {
		c.Validation.MinSize(imgFile, 2*KB).
			Message("Minimum a file size of 2KB expected")
		c.Validation.MaxSize(imgFile, 2*MB).
			Message("File cannot be larger than 2MB")

		// Check format of the file.
		conf, format, err := image.DecodeConfig(bytes.NewReader(imgFile))
		c.Validation.Required(err == nil).Key("imgFile").
			Message("Incorrect file format")
		c.Validation.Required(format == "jpeg" || format == "png").Key("imgFile").
			Message("JPEG or PNG file format is expected")

		// Check resolution.
		c.Validation.Required(conf.Height >= 150 && conf.Width >= 150).Key("imgFile").
			Message("Minimum allowed resolution is 150x150px")

		// Handle errors.
		if c.Validation.HasErrors() {
			c.Validation.Keep()
			c.FlashParams()
			return c.Redirect(NewsController.Index)
		}

		//create destination file making sure the path is writeable.
		dstPath := "public/img/post/" + c.Params.Files["imgFile"][0].Filename + time.Now().UTC().Format(time.UnixDate)
		dst, err := os.Create(dstPath)
		defer dst.Close() //close the destination file handle on function return
		if err != nil {
			log.Print(err)
		}
		io.Copy(dst, bytes.NewReader(imgFile))

		// Copied
		news = addNews(f.Get("title"), f.Get("context"), dstPath, "User")
	} else {
		news = addNews(f.Get("title"), f.Get("context"), "", "User")
	}

	err := app.Dbmap.Insert(&news)
	app.CheckErr(err, "Insert Failed.")

	c.Flash.Success("News added.")

	return c.Redirect(NewsController.Index)
}

func addNews(title string, context string, imagePath string, username string) models.News {
	return models.News{
		Title:     title,
		Context:   context,
		ImagePath: imagePath,
		CreatedAt: time.Now().UTC().Format(time.UnixDate),
		UpdatedAt: time.Now().UTC().Format(time.UnixDate),
		CreatedBy: username,
		UpdatedBy: username,
	}
}

func (c NewsController) DeleteNews(id int) revel.Result {
	var SelectedNews models.News
	err := app.Dbmap.SelectOne(&SelectedNews, "SELECT * FROM news WHERE news_id = ?", id)
	app.CheckErr(err, "No result found")
	_, err = app.Dbmap.Delete(&SelectedNews)
	app.CheckErr(err, "Couldn't delete the News")
	c.Flash.Success("Successfully deleted.")
	return c.Redirect(NewsController.Index)
}

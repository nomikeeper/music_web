import React, { Component } from "react";
import ReactDOM from "react-dom";
// Importing Pagination 
import Pagination from "react-js-pagination";

const states = {
    All_News:"All_News",
    Add_News: "Add_News", 
    Edit_News: "Edit_News"
}

class News extends Component {
    constructor(props){
        super(props);
        this.state={
            currentState: states.All_News,
            currentNews: null,
            isInitialFetch: false,
            isFetching: false,
            totalNewsCount: null,
            itemPerPage: 5,
            activePage: 1,
            editForm:{
                title: null,
                context: null
            }
        }
        String.prototype.trunc =
            function( n, useWordBoundary ){
                if (this.length <= n) { return this; }
                var subString = this.substr(0, n-1);
                return (useWordBoundary 
                    ? subString.substr(0, subString.lastIndexOf(' ')) 
                    : subString) + "...";
            };
    }

    componentWillMount(){
        if(!this.state.isInitialFetch){
            fetch("http://localhost:9000/FetchNews/1")
            .then(resp => resp.json())
            .then(
                (result) => {
                    this.setState({ 
                        totalNewsCount : result.total_count,
                        currentNews: result.news_list
                    })
                },
                (error) => {
                    console.error("Error: ", error)
                }
            )
        }   
    }
    
    // State changer function
    stateChanger(isEdit){
        if(this.state.currentState == states.Add_News || this.state.currentState == states.Edit_News)
        {
            this.setState({currentState: states.All_News})
        }
        else{
            if(!isEdit)
                this.setState({currentState: states.Add_News})
            else
                this.setState({currentState: states.Edit_News})
        }
    }

    // convert Revel stringified bytes array to js bytes array
    // Revel stringified byte array looks like [123 34 107 101 121 34 58 34 118 97 108 117 101 34 125]
    // JS acceptable stringified byte array looks like [123, 34, 107, 101, 121, 34, 58, 34, 118, 97, 108, 117, 101, 34, 125]
    StringToArray(str){
        let FirstRemoval = str.split("[");
        let SecondRemoval = FirstRemoval[1].split("]");
        return SecondRemoval[0].split(" ")
    }

    // String to Array
    SetAllNews(str){
        // Convert the array
        let LastData = this.StringToArray(str)
        // Converting object to string
        let realData = String.fromCharCode.apply(String, LastData);
        // Parsing string into JSON Object
        let parsedData = JSON.parse(realData)
        this.setState({ 
            currentNews: parsedData,
            isAllNewsFetched: true,
        })
    }
    // Title selector
    titleSelector(){
        switch(this.state.currentState){
            case states.Add_News:
                return "Add News";
            case states.All_News:
                return "All News";
            case states.Edit_News:
                return "Edit News";
            default:
                return "5o5!"
        }
    }

    // Content Selector
    contentSelector(){
        switch(this.state.currentState){
            case states.Add_News:
                return (
                    <form action="/AddNews" method="POST" id="news-form" encType="multipart/form-data">
                        <label htmlFor="title">Title</label>
                        <input type="text" className="title-input" name="title" />
                        <label htmlFor="context">Context</label>
                        <textarea name="context" className="context-input"></textarea>
                        <label htmlFor="image">Image</label>
                        <input type="file" name="imgFile" defaultValue="Choose Image" accept="image/*"/>
                        <input type="submit" value="Publish" className="publish-btn" />
                    </form>
                );
            case states.All_News:
                return (
                    this.state.totalNewsCount == 0 || this.state.totalNewsCount == null ?
                        <h2>There is no news right now.</h2>
                        :
                        <ul>
                            <li>
                                <div className="item-index"></div>
                                <div className="item-title center">Title</div>
                                <div className="item-context center">Context</div>
                                <div className="item-date">Date</div>
                                <div className="center">
                                    Controls
                                </div>
                            </li>
                            { this.state.currentNews.map((item, index) => {
                                return(
                                    <li key={index}>
                                        <div className="item-index">{ index + 1}</div>
                                        <div className="item-title">{item.news_title}</div>
                                        <div className="item-context">
                                            {
                                                item.news_context.trunc(50, true)
                                            }
                                        </div>
                                        <div className="item-date">
                                            { moment(item.news_updated_at).format("LL") }
                                            {/* <script type="text/javascript">
                                                var date =  {{ $value.UpdatedAt }};
                                                document.writeln(moment(date).format("LL"));
                                            </script> */}
                                        </div>
                                        <div className="controls">
                                            <div className="action-btn">
                                                <a href="#" className="far fa-edit"></a>
                                                <span>Edit</span>
                                            </div>
                                            <div className="action-btn">
                                                <a href="/DeleteNews/{{$value.NewsID}}" className="far fa-trash-alt"></a>
                                                <span>Delete</span>
                                            </div>
                                        </div>
                                    </li>
                                );
                            })}
                        </ul>
                );
            case states.Edit_News:
                return "Edit News";
            default:
                return "There was an error!"
        }
    }

    // Pagination handler
    handlePageChange(pageNumber) {
        fetch(`http://localhost:9000/FetchNews/${pageNumber}`)
        .then((res) => res.json())
        .then(
            (result) => {
                this.setState({ 
                    currentNews: result.news_list
                })
            },
            (error) => {
                console.log("Error happened", error)
            }
        )

        this.setState({activePage: pageNumber});
      }
     
    // Pagination builder
    paginationBuilder(){
        console.log("This.state >", this.state)
        return(
            <div className="paginator">
                <Pagination
                    activeClass="active"
                    activePage={this.state.activePage}
                    itemsCountPerPage={this.state.itemPerPage }
                    totalItemsCount={ this.state.totalNewsCount }
                    pageRangeDisplayed={ 5}
                    firstPageText="Start"
                    lastPageText="Last"
                    onChange={(num) => this.handlePageChange(num)}
                />
            </div>
        )
    }
    // Footer selector
    footerSelector(){
        switch(this.state.currentState){
            case states.Add_News:
                return null;
            case states.All_News:
                return (
                    this.paginationBuilder()
                );
            case states.Edit_News:
                return "Edit News";
            default:
                return "There was an error!"
        }
    }

    // Button Content
    buttonContentSelector(){
        if(this.state.currentState == states.Add_News || this.state.currentState == states.Edit_News)
        {
            return <i class="fas fa-times"></i>
        }
        else{
            return <i className="fas fa-plus"></i>
        }
    }

    render(){
        //let _data = document.getElementById("_react_component").getAttribute("data-news");
        return(
        <div className="content-container">

            <div className="title">
                <h1>{ this.titleSelector() }</h1>
            </div>

            <div id="content">
                    { this.contentSelector() }
            </div>

            {this.footerSelector()}

            <div className="add-news">
                <a onClick={() => this.stateChanger()}>
                    { this.buttonContentSelector() }
                </a>
            </div>
        </div>
        );
    }
}

export default ReactDOM.render(<News />, document.getElementById("_react_component"))